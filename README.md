# [frag.jetzt Landing Page](https://frag-nicht.herokuapp.com/)

This is my landing page for the group project frag.jetzt in line with WBS-Testat #3.

## Preview

[![New Age Preview](img/Screenshot.png)](https://frag-nicht.herokuapp.com/)

**[View Live on Herokuapp](https://frag-nicht.herokuapp.com/)**


## Download and Installation

To begin using this landing page, choose one of the following options to get started:

* [Download the latest release on Start Bootstrap](https://startbootstrap.com/themes/new-age/)
* Clone the repo: `git clone git@git.thm.de:brnk73/frag-nicht.git`
* Install via npm: `npm install`
* [Fork, Clone, or Download on GitHub](https://git.thm.de/brnk73/frag-nicht)

## Usage

### Basic Usage

After downloading, simply edit the HTML and CSS files included with the template in your favorite text editor to make changes. These are the only files you need to worry about, you can ignore everything else! To preview the changes you make to the code, you can open the `index.html` file in your web browser.

### Advanced Usage

After installation, run `npm install` and then run `npm start` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved. You can view the `gulpfile.js` to see which tasks are included with the dev environment.

#### Gulp Tasks

* `gulp` the default task that builds everything
* `gulp watch` browserSync opens the project in your default browser and live reloads when changes are made
* `gulp css` compiles SCSS files into CSS and minifies the compiled CSS
* `gulp js` minifies the themes JS file
* `gulp vendor` copies dependencies from node_modules to the vendor directory

You must have npm installed globally in order to use this build environment.

## Copyright and License

Copyright 2013-2020 Start Bootstrap LLC. Code released under the [MIT](https://github.com/StartBootstrap/startbootstrap-new-age/blob/gh-pages/LICENSE) license.
